import pandas as pd

def domain_word_count(text, domain_amount, domain_separator):
    symbols_to_delete = '.,!?-:;()\'"'

    word_count = {}

    domain = text.split(domain_separator)[1:]  
    domain_counts = {i: 0 for i in range(1, domain_amount + 1)}

    for chapter_index, chapter in enumerate(domain):
        domain = (chapter_index // 2) + 1
        words_list = chapter.split()

        for word in words_list:
            word = word.strip(symbols_to_delete).lower()
            word_length = len(word)
            
            for i in range(1, word_length + 1):
                prefix = word[:i]
                if prefix not in word_count:
                    word_count[prefix] = {'count': 0, 'domains': domain_counts.copy()}
                word_count[prefix]['count'] += 1
                word_count[prefix]['domains'][domain] += 1

    return word_count

def data_frame_from_word_count(word_count, domain_amount):
    data = []

    for prefix, other in word_count.items():
        total_count = other['count']
        domain_counts = [other['domains'][i] for i in range(1, domain_amount + 1)]
        data.append([list(prefix), prefix, total_count] + domain_counts)
 
    columns = ['Ключ', '"Слово"', 'Общее количество'] + ['Количество в домене {}'.format(i) for i in range(1, domain_amount + 1)]
    df = pd.DataFrame(data, columns=columns)
    return df

# Тестовый текст
with open('./Тестовый текст.txt', 'r', encoding='utf-8') as file:
    text = file.read()

otci_i_deti_domain_separator = 'Глава '
otci_i_deti_domain_amount = 1

word_count = domain_word_count(text, otci_i_deti_domain_amount, otci_i_deti_domain_separator)
df = data_frame_from_word_count(word_count, otci_i_deti_domain_amount)
df.to_excel("test_text.xlsx", index=False)

# Отцы и дети
with open('./Отцы и дети.txt', 'r', encoding='utf-8') as file:
    text = file.read()

otci_i_deti_domain_separator = 'Глава '
otci_i_deti_domain_amount = 14

word_count = domain_word_count(text, otci_i_deti_domain_amount, otci_i_deti_domain_separator)
df = data_frame_from_word_count(word_count, otci_i_deti_domain_amount)
df.to_excel("otci_i_deti.xlsx", index=False)

# Капитанская дочка
with open('./Капитанская дочка.txt', 'r', encoding='utf-8') as file:
    text = file.read()

kap_dotchka_domain_separator = 'ГЛАВА '
kap_dotchka_domain_amount = 7

word_count = domain_word_count(text, kap_dotchka_domain_amount, kap_dotchka_domain_separator)
df = data_frame_from_word_count(word_count, kap_dotchka_domain_amount)
df.to_excel("kapitanskaya_dochka.xlsx", index=False)